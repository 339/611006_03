<?php
require_once 'vendor/autoload.php';

$std_dep = 'ระบบสารสนเทศและเครือข่าย';
$std_name = 'นายพรณพล      ปุญญพันธุ์';
$std_id = '580611039';
$type = 'วศ1';
$score = '50';
$date = '20 ต.ค. 2570';
//$type2 = NULL;
$type2 = 'วศ2';
$score2 = '73';
$date2 = '21 ม.ค 2575';
$todaydate = '21 ม.ค 2575';

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$dec1 = array( 
    'align' => 'center','spaceBefore' => 0, 'spaceAfter' => 120 
);

$dec2 = array( 
    'align' => 'both','spaceBefore' => 0, 'spaceAfter' => 0 
);

$dec3 = array( 
    'align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0 , 'indentation' => array('left' => 3402, 'right' => -2)
);

$section = $phpWord->addSection(
    array('marginLeft' => 1701, 'marginRight' => 1134,
     'marginTop' => 851, 'marginBottom' => 851)
  );

$section->addImage(
    'images/image1.png',
    array(
        'width'         => 98.05,
        'height'        => 85,
        'align'=>'center'      
    )
);

$section->addText(
    'แบบฟอร์มส่งผลสอบภาษาต่างประเทศ',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true) 
    ,$dec1 
);

if($type2 == NULL || $score2 == NULL || $date2 == NULL) {
    $textrun = $section->createTextRun($dec2);
    $textrun->addText(
        htmlspecialchars("\t").
        'ข้าพเจ้า'.
        htmlspecialchars("\t"),
        array('name' => 'TH SarabunIT๙', 'size' => 16)
    );
    $textrun->addText(
        $std_name.
        htmlspecialchars("\t"),
        array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true)
    );
    $textrun->addText(
        'รหัสนักศึกษา'.
        htmlspecialchars("\t"),
        array('name' => 'TH SarabunIT๙', 'size' => 16)
    );
    $textrun->addText(
        $std_id.
        htmlspecialchars("\t"),
        array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true)
    );
    $textrun->addText(
        'สาขาวิชา '.$std_dep. 
        ' คณะวิศวกรรมศาสตร์ ขอส่งผลสอบภาษาต่างประเทศชนิด '.$type. 
        ' ระดับคะแนน '.$score. 
        ' เมื่อวันที่ '.$date,
        array('name' => 'TH SarabunIT๙', 'size' => 16)
    );
}
else {
    $textrun = $section->createTextRun($dec2);
    $textrun->addText(
        htmlspecialchars("\t").
        'ข้าพเจ้า'.
        htmlspecialchars("\t\t"),
        array('name' => 'TH SarabunIT๙', 'size' => 16)
    );
    $textrun->addText(
        $std_name.
        htmlspecialchars("\t"),
        array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true)
    );
    $textrun->addText(
        'รหัสนักศึกษา'.
        htmlspecialchars("\t"),
        array('name' => 'TH SarabunIT๙', 'size' => 16)
    );
    $textrun->addText(
        $std_id.
        htmlspecialchars("\t"),
        array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true)
    );
    $textrun->addText(
        'สาขาวิชา '.$std_dep. 
        ' คณะวิศวกรรมศาสตร์ ขอส่งผลสอบภาษาต่างประเทศชนิด '.$type. 
        ' ระดับคะแนน '.$score. 
        ' เมื่อวันที่ '.$date.  
        ' และ '.$type2. 
        ' ระดับคะแนน ' .$score2. 
        ' เมื่อวันที่ '.$date2,
        array('name' => 'TH SarabunIT๙', 'size' => 16)
    );
}

$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec3
);

$section->addText(
    'ลงชื่อ ................................................',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec3
);

$section->addText(
    '('.$std_name.')',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec3
);

$section->addText(
    $todaydate,
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec3
);

$section->addImage(
    'images/image2.png',
    array(
        'width'         => 310.8,
        'height'        => 418.8,
        'wrappingStyle' => 'infront',
        'align'=>'center',
        'wrappingStyle'=>'square'
    )
);

$section->addImage(
    'images/image3.png',
    array(
        'width'         => 374.55,
        'height'        => 529.65,
        'wrappingStyle' => 'infront',
        'align'=>'center',
        'wrappingStyle'=>'square'
    )
);

// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('C:\xampp\htdocs\project192\resources\export_611006\611006_03_std.docx');
