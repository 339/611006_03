<?php
require_once 'vendor/autoload.php';

$department = 'วิศวกรรมเครื่องกล';
$tel = '42006 ต่อ 415';
$document_no = 'ศธ6593(14).2/  ';
$document_date = '26    สิงหาคม 2561';
$std_dep = array('วิศวกรรมเครื่องกล', 'การจัดการอุตสาหกรรม');
$std_name = array(array('นายธนพล ยาฉาย'), array('นางสาวพิชญา องค์ศิริมงคล'));
$std_id = array(array('610651002'), array('610632059'));
$std_plan = array(array('แบบ 1.1 ฐาน ป.โท'), array('แบบ 2'));
$std_count = 0;

$head = 'รองศาสตราจารย์ ดร.ธงชัย ฟองสมุทร';
$head_pst = 'หัวหน้าภาควิชาวิศวกรรมเครื่องกล';
for($i = 0; $i<count($std_dep); $i++) {
    $std_count += count($std_name[$i]);
}

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$section = $phpWord->addSection(
    array('marginLeft' => 1701, 'marginRight' => 1134,
     'marginTop' => 851, 'marginBottom' => 851)
  );

$dec1 = array( 
    'align' => 'center','spaceBefore' => 0, 'spaceAfter' => 120 
);

$dec2 = array( 
    'align' => 'both', 'spaceAfter' => 0 , 'spaceBefore' => 120 
);

$dec3 = array( 
    'align' => 'both', 'spaceAfter' => 0 , 'spaceBefore' => 0 
);

$dec4 = array( 
    'align' => 'left', 'spaceAfter' => 0 , 'spaceBefore' => 240 
);

$dec5 = array( 
    'align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0 , 'indentation' => array('left' => 3402, 'right' => -2)
);

$textrun = $section->createTextRun();
$textrun->addImage(
    'images/image4.png',
    array(
        'width'         => 42.4,
        'height'        => 42.4,
        'align'         =>'left',
    )
);

$textrun->addText(
    htmlspecialchars("\t\t\t\t").
    'บันทึกข้อความ',
    array('name' => 'TH SarabunIT๙', 'size' => 29 , 'bold' => true) 
    ,$dec1 
);

$textrun = $section->createTextRun($dec3);
$textrun->addText(
    'ส่วนงาน',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    htmlspecialchars("\t").
    'ภาควิชา'.
    $department.
    htmlspecialchars("\t").
    'คณะวิศวกรรมศาสตร์  '.
    htmlspecialchars("\t").
    'โทร. '. 
    $tel,
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted' )
);

$textrun = $section->createTextRun($dec3);
$textrun->addText(
    'ที่',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    htmlspecialchars("\t").
    $document_no.
    htmlspecialchars("\t\t\t"),
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted' )
);
$textrun->addText(
    'วันที่',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    htmlspecialchars("\t").
    $document_date,
    array('name' => 'TH SarabunIT๙', 'size' => 16 ,'underline' => 'dotted' )
);

$textrun = $section->createTextRun($dec3);
$textrun->addText(
    'เรื่อง',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    '   ขอส่งผลการผ่านเงื่อนไขภาษาต่างประเทศสำหรับนักศึกษาบัณฑิตศึกษา	',
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec4);
$textrun->addText(
    'เรียน',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true)
);
$textrun->addText(
    htmlspecialchars("\t").
    'คณบดีบัณฑิตวิทยาลัย',
    array('name' => 'TH SarabunIT๙', 'size' => 16)
);

$section->addText(
    htmlspecialchars("\t\t").
    'ตามที่ภาควิชา'.
    $department.
    'ได้รับเอกสารผลการสอบผ่านเงื่อนไขภาษาต่างประเทศสำหรับนักศึกษาระดับบัณฑิตศึกษา'.
    ' จำนวน '.
    $std_count.
    ' ราย ได้แก่',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec2
);

for($i = 0; $i < count($std_dep); $i++) {
    $section->addText(
        htmlspecialchars("\t\t").
        'สาขาวิชา'.
        $std_dep[$i],
        array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true ),
        $dec2
    );
    for($j = 0; $j <count($std_name[$i]); $j++){
    $section->addText(
        htmlspecialchars("\t\t").
        ($j+1).
        '. '.
        $std_name[$i][$j].
        htmlspecialchars("\t\t").
        'รหัสนักศึกษา '.
        $std_id[$i][$j].
        htmlspecialchars("\t").
        $std_plan[$i][$j],
        array('name' => 'TH SarabunIT๙', 'size' => 16,),
        $dec3
    );
    }
}

$section->addText(
    htmlspecialchars("\t\t").
    'ในการนี้ ภาควิชา'.
    $department.
    ' ใคร่ขอส่งเอกสารดังกล่าว ให้คณะวิศวกรรมศาสตร์ และบัณฑิตวิทยาลัยเพื่อใช้เป็นหลักฐานประกอบการศึกษา ทั้งนี้ได้แนบเอกสารมาพร้อมนี้',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec2
);

$section->addText(
    htmlspecialchars("\t\t").
    'จึงเรียนมาเพื่อโปรดพิจารณาดำเนินการต่อไป',
     array('name' => 'TH SarabunIT๙', 'size' => 16),
     $dec2
);

$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec3
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec3
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec3
);
$section->addText(
    '('.$head.')',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec5
);
$section->addText(
    $head_pst,
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec5
);

// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('C:\xampp\htdocs\project192\resources\export_611006\611006_03_sub.docx');
