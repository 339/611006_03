<?php
require_once 'vendor/autoload.php';

$document_no = 'ศธ ๖5๙๓(๑๔)/xxxx';
$document_date = '๒๖  กันยายน   ๒๕๖๑';
$std_dep = array('วิศวกรรมเครื่องกล', 'การจัดการอุตสาหกรรม');
$std_name = array(array('นายธนพล ยาฉาย'), array('นางสาวพิชญา องค์ศิริมงคล'));
$std_id = array(array('610651002'), array('610632059'));
$std_plan = array(array('แบบ 1.1 ฐาน ป.โท'), array('แบบ 2'));
$std_count = 0;

$head = 'รองศาสตราจารย์ ดร.พฤทธ์  สกุลช่างสัจจะทัย';
$head_pst = 'รองคณบดีฝ่ายวิจัย บริการวิชาการและบัณฑิตศึกษา';
$head_pst2 = 'ปฏิบัติการแทนประธานกรรมการบัณฑิตศึกษาประจำคณะวิศวกรรมศาสตร์';

for($i = 0; $i<count($std_dep); $i++) {
    $std_count += count($std_name[$i]);
}
// Creating the new document...
$phpWord = new \PhpOffice\PhpWord\PhpWord();

$dec1 = array( 
    'align' => 'left','spaceBefore' => 0, 'spaceAfter' => 120 
);

$dec2 = array( 
    'align' => 'both','spaceBefore' => 120 , 'spaceAfter' => 0
);

$dec3 = array( 
    'align' => 'left', 'space' => array('before' => 0, 'after' => 0)
);

$dec4 = array( 
    'align' => 'center','spaceBefore' => 0 , 'spaceAfter' => 0 , 'indentation' => array('left' => 3402, 'right' => -2)
);

$dec5 = array( 
    'align' => 'left', 'space' => array('before' => 240, 'after' => 0)
);

$section = $phpWord->addSection(
    array('marginLeft' => 1701, 'marginRight' => 1134,
     'marginTop' => 851, 'marginBottom' => 851)
  );

$textrun = $section->createTextRun($dec1);
$textrun->addImage(
    'images/image4.png',
    array(
        'width'         => 42.4,
        'height'        => 42.4,
        'align'         =>'left',
        'wrappingStyle' => 'square',
    )
);
$textrun->addText(
    htmlspecialchars("\t\t\t\t").
    'บันทึกข้อความ',
    array('name' => 'TH SarabunIT๙', 'size' => 29, 'bold'=>true) 
);

$textrun = $section->createTextRun($dec3);
$textrun->addText(
    'ส่วนงาน',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    '   งานบริการการศึกษาและพัฒนาคุณภาพนักศึกษา คณะวิศวกรรมศาสตร์ (โทร.๔๑๘๐ ต่อ 116)',
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec3);
$textrun->addText(
    'ที่',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    htmlspecialchars("\t\t").$document_no.
    htmlspecialchars("\t\t\t"),
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);
$textrun->addText(
    'วันที่',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    $document_date.
    htmlspecialchars("\t\t"),
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec3);
$textrun->addText(
    'เรื่อง',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    '   ขอส่งผลการผ่านเงื่อนไขภาษาต่างประเทศนักศึกษาบัณฑิตศึกษา คณะวิศวกรรมศาสตร์	',
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec5);
$textrun->addText(
    'เรียน ',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true)
);
$textrun->addText(
    'คณบดีบัณฑิตวิทยาลัย',
    array('name' => 'TH SarabunIT๙', 'size' => 16)
);

$section->addText(
    htmlspecialchars("\t\t").
    'ด้วยคณะวิศวกรรมศาสตร์ มีความประสงค์ ขอส่งผลการผ่านเงื่อนไขภาษาต่างประเทศนักศึกษาบัณฑิตศึกษา คณะวิศวกรรมศาสตร์ จำนวน '.
     $std_count.
     ' ราย   ดังนี้',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec2
);
for($i = 0; $i < count($std_dep); $i++) {
    $section->addText(
        htmlspecialchars("\t\t").
        'สาขาวิชา'.
        $std_dep[$i],
        array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true ),
        $dec2
    );
    for($j = 0; $j <count($std_name[$i]); $j++){
    $section->addText(
        htmlspecialchars("\t\t").
        ($j+1).
        '. '.
        $std_name[$i][$j].
        htmlspecialchars("\t\t").
        'รหัสนักศึกษา '.
        $std_id[$i][$j].
        htmlspecialchars("\t").
        $std_plan[$i][$j],
        array('name' => 'TH SarabunIT๙', 'size' => 16),
        $dec3
    );
    }
}

$section->addText(
    htmlspecialchars("\t\t").
    'จึงเรียนมาเพื่อโปรดพิจารณาดำเนินการต่อไป',
    array('name' => 'TH SarabunIT๙', 'size' => 16,),
    $dec3
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec3
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec3
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec3
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec3
);
$section->addText(
    '('.$head.')',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec4
);
$section->addText(
    $head_pst,
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec4
);
$section->addText(
    $head_pst2,
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec4
);

// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('C:\xampp\htdocs\project192\resources\export_611006\611006_03_adm_1.docx');
?>